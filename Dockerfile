###############################
##
## Dockerfile for the homalg release
##
## Creates a container with arch linux, Polymake, Singular, GAP, and homalg packages as git repositories.
##
###############################

FROM sebasguts/arch_for_homalg

MAINTAINER Sebastian Gutsche

ADD install_files /install_files

RUN \
    ## Install gap (crucial)
    cd /tmp && wget ftp://ftp.gap-system.org/pub/gap/gap45/tar.bz2/gap4r5p6_2012_12_09-19_28.tar.bz2 && \
    tar -xf gap4r5*tar* && sudo mv gap4r5 /opt/ && sudo chown -R homalg /opt/gap4r5 && rm gap*tar* && \
    cd /opt/gap4r5 && ./configure --with-gmp=system && make config && make -j && \
    cd pkg/io/ && ./configure && make && \
    cd ../Browse && ./configure && make && cd .. && \
    rm -rf 4ti2Interface/ AutoDoc/ Convex/ ExamplesForHomalg/ Gauss GaussForHomalg/ GradedModules/ GradedRingForHomalg/ homalg/ HomalgToCAS/ IO_ForHomalg/ LocalizeRingForHomalg/ MatricesForHomalg/ PolymakeInterface/ RingsForHomalg/ SCO/ ToolsForHomalg/ ToricVarieties/ && \
    cd .. && mkdir local && cd local && mkdir pkg && cd pkg && \
    cp /install_files/pull_packages . && ./pull_packages && \
    cd /opt/gap4r5 && cd bin && ln -snf gap-default64.sh gap.sh && cd .. && cp /install_files/create_workspace . && ./create_workspace && \
    sudo bash -c "echo '/opt/gap4r5/bin/gap.sh -l \"/opt/gap4r5/local;/opt/gap4r5\" \"\$@\"' > /usr/bin/gap" && \
    sudo bash -c "echo '/opt/gap4r5/bin/gap.sh -l \"/opt/gap4r5/local;/opt/gap4r5\" -L /opt/gap4r5/bin/wsgap4 \"\$@\"' > /usr/bin/gapL" && \
    sudo chmod +x /usr/bin/gap && \
    sudo chmod +x /usr/bin/gapL

ENV HOME /home/homalg

VOLUME /home/homalg/FCL

ENTRYPOINT /bin/bash 
